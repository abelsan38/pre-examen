// const { default: axios } = require("axios")
const formulario = document.getElementById('formulario');
const res = document.getElementById('res');
formulario.addEventListener('submit', e => {
    e.preventDefault();
    const valor = document.getElementById('valor').value;
    llamadaConAxios(valor);
})


const llamadaConAxios = async (user) => {
    try {
        const respuesta = await axios(`https://jsonplaceholder.typicode.com/users/${user}`);
        mostrar(respuesta);
    } catch (error) {
        const h1 = document.createElement('h1')
        h1.innerText = 'No se pudieron encontrar los datos'
        h1.classList.add('text-danger', 'p-1')
        res.appendChild(h1)
    }

}
function mostrar(datos) {
    eliminar()
    const h1 = document.createElement('h1');
    const h2 = document.createElement('h1');
    const h3 = document.createElement('h1');
    const h4 = document.createElement('h1');
    const h5 = document.createElement('h1');
    const h6 = document.createElement('h1');

    h1.innerText = `${datos.data.name}`;
    h2.innerText = `Username: ` + datos.data.username;
    h3.innerText = `Email: ` + datos.data.email;
    h4.innerText = `Calle: ` + datos.data.address.street;
    h5.innerText = `Numero: ` + datos.data.address.suite;
    h6.innerText = 'Ciudad: ' + datos.data.address.city;

    // Add Bootstrap classes to the elements
    h1.classList.add('card-title', 'text-primary');
    h2.classList.add('card-title', 'text-dark');
    h3.classList.add('card-title', 'text-dark');
    h4.classList.add('card-title', 'text-dark');
    h5.classList.add('card-title', 'text-dark');
    h6.classList.add('card-title', 'text-dark');

    // Create a div to act as the card container
    const cardDiv = document.createElement('div');
    cardDiv.classList.add('card', 'mt-3', 'shadow');

    // Append the elements to the card container
    cardDiv.appendChild(h1);
    cardDiv.appendChild(h2);
    cardDiv.appendChild(h3);
    cardDiv.appendChild(h4);
    cardDiv.appendChild(h5);
    cardDiv.appendChild(h6);

    // Append the card container to the result container
    res.appendChild(cardDiv);


}
function eliminar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild);
    }
}
