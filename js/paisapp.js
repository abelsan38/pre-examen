const form = document.getElementById('form');
const res = document.getElementById('res');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    const valor = document.getElementById('pais').value;

    fetchApiPais(valor)
})

async function fetchApiPais(valor) {
    const url = `https://restcountries.com/v3.1/name/${valor}?fullText=true`
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        mostrarResultado(resultado);
    } catch (error) {
        const h1 = document.createElement('h1')
        h1.innerText = 'No se pudieron encontrar los datos'
        h1.classList.add('text-danger', 'p-1')
        res.appendChild(h1)

    }

}
function mostrarResultado(valor) {
    eliminar()
    valor.forEach(pais => {
        // console.log(pais.languages)
        const idiomas = Object.values(pais.languages)

        pais.capital.forEach(capital => {
            console.log(capital);
            const h1 = document.createElement('h1')
            h1.innerText = 'Capital: ' + capital
            res.appendChild(h1)

        })
        idiomas.forEach(idioma => {
            const h2 = document.createElement('h1')
            h2.innerText = "Lenguaje: " + idioma
            res.appendChild(h2)
        })

    });


}
function eliminar() {
    while (res.firstChild) {
        res.removeChild(res.firstChild);
    }
}